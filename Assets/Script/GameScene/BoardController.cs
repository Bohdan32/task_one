﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BoardController : MonoBehaviour 
{
    public static BoardController Instance;

    private CellInBoard [,] _cellInBoard = new CellInBoard[3,3];

    [SerializeField]
    private CellInBoard _cellBase;

    private EasyLevel _easyLevel = new EasyLevel();

    private MediumLevel _mediumLevel = new MediumLevel();

    private HardLevel _hardLevel = new HardLevel();

    private List<Vector2> emptyCell = new List<Vector2>();

    private float _widthCell;

    private float _paddindCell;

    private GameSession _gameSession;

    public GameSession GetGameSession { get { return _gameSession; } }

    private eTarget _botTarget;

    private int _botLevel;

    private float _timeWorkingBot;

    public int GetLevel
    {
        get
        { 
            return _botLevel;
        }
        set
        { 
            _botLevel = value;
        }
    }

    void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        _botLevel = ProfilePlayer.GetLevel;// This value check level bot(what level the user chose);

        switch (_botLevel)//How much time bot will think
        {
            case 1:
                _timeWorkingBot = ProfileBot.GetTimeWorkingBotEasy;
                break;
            case 2:
                _timeWorkingBot = ProfileBot.GetTimeWorkingBotMedium;
                break;
            case 3:
                _timeWorkingBot = ProfileBot.GetTimeWorkingBotHard;
                break;
        }

        _widthCell = _cellBase.transform.GetComponent<SpriteRenderer>().bounds.size.x;

        _paddindCell = _widthCell / 4;

        InstantiateCellInBoard();// Building cells in field 

        CheckWhoFirstMove(ProfilePlayer.GetLastRound);// Check, who must move first(Bot or Player);   
    }
 
    private void InstantiateCellInBoard()
    {  
        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                GameObject cell = Instantiate(_cellBase.gameObject, _cellBase.gameObject.transform.parent, false) as GameObject;

                cell.transform.parent = gameObject.transform;

                cell.transform.position = _cellBase.transform.position;

                cell.GetComponent<Transform>().position = (new Vector2(i * (_widthCell + _paddindCell) + _cellBase.GetComponent<Transform>().position.x,
                    -k * (_widthCell + _paddindCell) + _cellBase.GetComponent<Transform>().position.y));
            
                cell.name = k + ":" + i;

                CellInBoard viewCellInBoard = cell.GetComponent<CellInBoard>();

                viewCellInBoard.PositionX = k;

                viewCellInBoard.PositionY = i;

                viewCellInBoard.BoardController = this;

                _cellInBoard[i, k] = viewCellInBoard;

                cell.SetActive(true);
            }
        }
    }

    public bool CheckWinwinningCombination()//Functions check wins. 
    {
       for(int i = 0; i< 3; i++)
        {
            //Vertical
            if (_cellInBoard[i, 0].TargetCell == _cellInBoard[i, 1].TargetCell  && _cellInBoard[i, 1].TargetCell== _cellInBoard[i, 2].TargetCell
                && _cellInBoard[i, 2].TargetCell!= eTarget.empty)
            {
                return true; 
            }
            //Horizontal
            if (_cellInBoard[0, i].TargetCell == _cellInBoard[1, i].TargetCell && _cellInBoard[1, i].TargetCell == _cellInBoard[2, i].TargetCell &&
                _cellInBoard[0, i].TargetCell!= eTarget.empty)
            {
                return true;
            }
        }
      //Down from right to left
        if (_cellInBoard[0, 0].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[1, 1].TargetCell == _cellInBoard[2, 2].TargetCell &&
            _cellInBoard[0, 0].TargetCell!= eTarget.empty)
        {
            return true;
        }
        //Up from right to left
        if (_cellInBoard[0, 2].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[1, 1].TargetCell == _cellInBoard[2, 0].TargetCell &&
            _cellInBoard[2, 0].TargetCell!= eTarget.empty)
        {
            return true;
        }
        return false;
    }

    private void CheckDraw()//Functions check draw. Check, if there are free cells. If there are none - do not go anywhere, draw.
    {
            emptyCell.Clear();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (_cellInBoard[i, j].TargetCell == eTarget.empty)
                    {
                        emptyCell.Add(new Vector2(i, j));
                    }
                }
            }
            if (emptyCell.Count == 0)
            {
                ProfilePlayer.EditLastRound(0);

                UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

                UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
            }
     }
    public void CheckWhoFirstMove(int a)//Functions check who move first(Bot or Player). If bot - do first move.
    {
        switch (a)
        {
            case 1:
                _gameSession = new GameSession(new RunPlayer(), new Cross()); 
                _botTarget = eTarget.zerro;
                ProfileBot.EditMoveLastRound(2);
                break;

            case 2:
                _gameSession = new GameSession(new RunBot(), new Cross());
                _botTarget = eTarget.cross;
                ProfileBot.EditMoveLastRound(1);
                switch (_botLevel)
                {
                    case 1:
                        _easyLevel.BotEasyLevel(_botTarget , ref _cellInBoard);
                        break;
                    case 2:
                        _mediumLevel.BotMediumLevel(_botTarget, ref _cellInBoard);
                        break;
                    case 3:
                        _hardLevel.BotHardLevel(_botTarget, ref _cellInBoard);
                        break;
                }
                break;

            case 0:            
                if (ProfileBot.GetMoveLastRound == 2)
                {
                    _gameSession = new GameSession(new RunBot(), new Cross());
                    _botTarget = eTarget.cross;
                    ProfileBot.EditMoveLastRound(1);
                    switch (_botLevel)
                    {
                        case 1:
                            _easyLevel.BotEasyLevel(_botTarget, ref _cellInBoard);
                            break;
                        case 2:
                            _mediumLevel.BotMediumLevel(_botTarget, ref _cellInBoard);
                            break;
                        case 3:
                            _hardLevel.BotHardLevel(_botTarget, ref _cellInBoard);
                            break;
                    }
                }
                else
                {
                    if (ProfileBot.GetMoveLastRound ==1)
                    {
                        _gameSession = new GameSession(new RunPlayer(), new Cross());
                        ProfileBot.EditMoveLastRound(2);
                        _botTarget = eTarget.zerro;
                    }   
                }
                break;
        }  
    }

    public Enum GoRun(int pos_x , int pos_y)//Function moves player and bot
    {
        IGameSession _playerNow = _gameSession.StateSession;

        eTarget target = (eTarget)_gameSession.Run();

        switch (target)
        {
            case eTarget.cross:

                _cellInBoard[pos_y, pos_x].TargetCell = eTarget.cross;// write result moves 

                break;

            case eTarget.zerro:

                _cellInBoard[pos_y, pos_x].TargetCell = eTarget.zerro;

                break;
        }

        if (CheckWinwinningCombination() && _playerNow.TargetSesion == eTargetSession.player)// Wins Player
        {
            ProfilePlayer.EditLastRound(1);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(1);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

            return target;
        }

        if (CheckWinwinningCombination() && _playerNow.TargetSesion == eTargetSession.bot)// Wins Bot
        {
            ProfilePlayer.EditLastRound(2);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(2);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

            return target;
        }
        if (_playerNow.TargetSesion == eTargetSession.bot)
        {
            CheckDraw();//After move, we must check draw, because there may not be free cells(It was the last move). 
        }

        if (_gameSession.StateSession is RunBot)
        {
            StartCoroutine(IRunBot(_timeWorkingBot));//Bot control delay
        }
       
        return target;
    }

    private IEnumerator IRunBot(float delay)
    {
        yield return new WaitForSeconds(delay);

        switch (_botLevel)
        {
            case 1:
                _easyLevel.BotEasyLevel(_botTarget, ref _cellInBoard);
                break;
            case 2:
                _mediumLevel.BotMediumLevel(_botTarget, ref _cellInBoard);
                break;
            case 3:
                _hardLevel.BotHardLevel(_botTarget, ref _cellInBoard);
                break;
        }      
    }


    //Next 2 state change state player and bot: if one of them move croos, other automatically receives zerro. Also we change of moves. 
    // State - 2 
    public interface IRun
    {
        eTarget Run(Run run);
    }

    public class Zerro : IRun
    {
        public eTarget Run(Run run)
        {
            run.State = new Cross();
            return eTarget.zerro;
        }
    }

    public class Cross : IRun
    {
        public eTarget Run(Run run)
        {
            run.State = new Zerro();
            return eTarget.cross;
        }  
    }

    public class Run
    {
        public IRun State { get; set; }

        public Run(IRun run)
        {
            State = run;
        }

        public eTarget ValueRun()
        {
          return State.Run(this); 
        }
    }

    // State - 1 
    public class GameSession
    {
        public IGameSession StateSession{get;set;}

        public Run run;

        public GameSession(IGameSession stateSession , IRun stateRun)
        {
            StateSession = stateSession;

            run = new Run(stateRun);
        }

        public Enum Run()
        {
            StateSession.Run(this); 

            return run.ValueRun();
        }
    }

    public interface IGameSession
    {
        void Run(GameSession player );

        eTargetSession TargetSesion { get;}
    }

    public class RunPlayer : IGameSession
    {
        public void Run(GameSession player)
        {
            player.StateSession = new RunBot();
        }

        public eTargetSession TargetSesion
        {
            get{ return eTargetSession.player;}
        }
    }

    public class RunBot : IGameSession
    {
        public void Run(GameSession player)
        {
            player.StateSession = new RunPlayer();
        }


        public eTargetSession TargetSesion
        {
            get { return eTargetSession.bot; }
        }
    }
}

public enum eTarget
{
    empty,
    cross,
    zerro,
}

public enum eTargetSession
{
    player,
    bot
}















