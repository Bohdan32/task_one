﻿using UnityEngine;
using UnityEngine.UI;

public class InformationPanels : MonoBehaviour 
{
    [SerializeField]
    private eTypePanel typePanel;

    [SerializeField]
    private Image _objInPanel;

    [SerializeField]
    private Sprite _spriteCross;

    [SerializeField]
    private Sprite _spriteZerro;

    [SerializeField]
    private Text _textCountWins;

    [SerializeField]
    private Text _textCountLose;

    [SerializeField]
    private Text _textCountDraw;

    void OnEnable()
    {
        GetInformation();

        GetIconCell(ProfilePlayer.GetLastRound);   
    }
        
    public void GetIconCell(int result)
    {   
        switch (result)
        {
            case 1://Wins player in last round
                if (typePanel == eTypePanel.BotPanel)
                {
                    _objInPanel.sprite = _spriteZerro;  
                }
                if (typePanel == eTypePanel.PlayerPanel)
                {
                    _objInPanel.sprite = _spriteCross;   
                }
                break;
            case 2://Wins bot in last round
                if (typePanel == eTypePanel.BotPanel)
                {
                    _objInPanel.sprite = _spriteCross;  
                }
                if (typePanel == eTypePanel.PlayerPanel)
                {
                    _objInPanel.sprite = _spriteZerro;  
                }
               
                break;

            case 0://draw in last round
                if (typePanel == eTypePanel.BotPanel && ProfileBot.GetMoveLastRound ==2)
                {
                    _objInPanel.sprite = _spriteCross;  
                }
                if (typePanel == eTypePanel.PlayerPanel && ProfileBot.GetMoveLastRound ==1)
                {
                    _objInPanel.sprite = _spriteZerro;  
                }

                break;
        }  
    }

    private void GetInformation()
    {
        if (typePanel == eTypePanel.BotPanel)
        {
            _textCountWins.text = ProfileBot.GetWin.ToString();

            _textCountLose.text = ProfileBot.GetLose.ToString();

            _textCountDraw.text = ProfileBot.GetDraws.ToString();
        }
        if (typePanel == eTypePanel.PlayerPanel)
        {
            _textCountWins.text = ProfilePlayer.GetWin.ToString();

            _textCountLose.text = ProfilePlayer.GetLose.ToString();

            _textCountDraw.text = ProfilePlayer.GetDraws.ToString();
        }
    }
}
public enum eTypePanel
{
    BotPanel,
    PlayerPanel

}
