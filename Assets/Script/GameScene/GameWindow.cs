﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameWindow : MonoBehaviour 
{
    [SerializeField]
    private Button _buttonStopGame;
	
	void Start () 
    {
        _buttonStopGame.onClick.AddListener(WillReturnMenu);
	}
	
    private void WillReturnMenu()
    {
        SceneManager.LoadScene("Menu");

        UnityPoolManager.Instance.ClearPool();
    }
	
}
