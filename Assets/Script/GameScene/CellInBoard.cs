﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CellInBoard : MonoBehaviour , IPointerDownHandler
{
    [SerializeField]
    private SpriteRenderer _cellSpriteRender;

    [SerializeField]
    private Sprite _spriteZero;

    [SerializeField]
    private Sprite _spriteCross;

    [SerializeField]
    private Sprite _spriteNormal;

    [SerializeField]
    private eState _wasClicked;

    private eTarget _targetCell;

    public eTarget TargetCell { get; set;}

    private int _сellValue = -1;

    public int GetCellValue
    {
        get
        {
            return _сellValue;
        }
    }

    public bool GoPlayer; 

    private int x;

    private int y;
	
    public int PositionX
    {
        get
        {       
            return x;
        }
        set
        { 
            x = value;
        }
    }

    public int PositionY
    {
        get
        {       
            return y;
        }
        set
        { 
            y = value;
        }
    }

    public BoardController BoardController{ get; set;}

    public void OnPointerDown(PointerEventData eventData)
    {
        //Click works only if the player must move. 
        if (BoardController.Instance.GetGameSession.StateSession.TargetSesion == eTargetSession.player)
        {
            SelectCell();
        }      
    }

    public void SelectCell()
    {
        if (_wasClicked == eState.NoClick)//Check repeated click
        {
            _wasClicked = eState.Click;

            eTarget target = (eTarget)BoardController.Instance.GoRun(PositionX, PositionY);

            switch (target)
            {
                case eTarget.cross:
                    //First we reduce the object. 
                    //When this is done we change sprite and show object.
                    gameObject.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.1f).OnComplete(() =>
                    {
                        _cellSpriteRender.sprite = _spriteCross;
                        gameObject.transform.DOScale(new Vector3(1f, 1f, 1f), 0.1f);
                    });
                    break;

                case eTarget.zerro:
                    gameObject.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.1f).OnComplete(() =>
                    {
                        _cellSpriteRender.sprite = _spriteZero;
                        gameObject.transform.DOScale(new Vector3(1f, 1f, 1f), 0.1f);
                    });
                    break;
            }
        }
        else
        {
            Debug.Log("Повторный клик!!!!!!!!!!!!!!!");
        }
    }

    public enum eState
    {
        NoClick,
        Click
    }
}
