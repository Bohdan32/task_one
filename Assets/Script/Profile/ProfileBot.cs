﻿public static class ProfileBot
{
    private static int _win;

    private static int _lose;

    private static int _draws;

    private static int _move_last_round;

    private static float _timeWorkingBotEasy = 0.1f;

    private static float _timeWorkingBotMedium = 0.5f;

    private static float _timeWorkingBotHard = 1f;

    static ProfileBot()
    {
        var ds = new DataService("Tic-tac-toe.db");

        var people = ds.GetBot();

        foreach (Bot person in people)
        {
            _win = person.win;    

            _lose = person.lose;  

            _draws = person.draws;  

            _move_last_round = person.move_first_last_round;  
        }
    }

    public static int GetWin
    {
        get
        {
            return _win;
        }
        set
        {
            _win = value;
        }
    }

    public static int GetLose
    {
        get
        {
            return _lose;
        }
        set
        {
            _lose = value;
        }
    }

    public static int GetDraws
    {
        get
        {
            return _draws;
        }
        set
        {
            _draws = value;
        }
    }

    public static int GetMoveLastRound
    {
        get
        {
            return _move_last_round;
        }
        set
        {
            _move_last_round = value;
        }
    }

    public static float GetTimeWorkingBotEasy
    {
        get
        {
            return _timeWorkingBotEasy;
        }
        set
        {
           _timeWorkingBotEasy = value;
        }
    }

    public static float GetTimeWorkingBotMedium
    {
        get
        {
            return _timeWorkingBotMedium;
        }
        set
        {
            _timeWorkingBotMedium = value;
        }
    }

    public static float GetTimeWorkingBotHard
    {
        get
        {
            return _timeWorkingBotHard;
        }
        set
        {
            _timeWorkingBotHard = value;
        }
    }

    public static void EditWin(int count)
    {
        GetWin = GetWin + count;

        var ds = new DataService("Tic-tac-toe.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }

    public static void EditLose(int count)
    {
        GetLose = GetLose + count;

        var ds = new DataService("Tic-tac-toe.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }

    public static void EditDraws(int count)
    {
        GetDraws = GetDraws + count;

        var ds = new DataService("Tic-tac-toe.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }

    public static void EditMoveLastRound(int count)
    {
        GetMoveLastRound = count;

        var ds = new DataService("Tic-tac-toe.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }
	
}
